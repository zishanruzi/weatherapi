const axios = require('axios');
const monsoondata = require('../model/monsoon');
require('dotenv').config();

const apiKey = process.env.API_KEY;
const apiUrl = process.env.API_URL;

exports.getData = async (req, res) => {
  const location = req.query.location;
  if (!location) {
    return res.status(400).send({ error: 'Location not provided' });
  }

  try {

    const existingWeatherData = await monsoondata.findOne({ name: { $regex: new RegExp(location, 'i') } });

    if (existingWeatherData) {
      const response = await axios.get(apiUrl, {
        params: {
          q: location,
          appid: apiKey,
        },
      });

      const weatherData = response.data;
      const { lon, lat } = weatherData.coord;

      weatherData.location = {
        type: 'Coordinate',
        coordinates: [lon, lat],
      };
      delete weatherData.coord;

     
      await monsoondata.updateOne({ name: { $regex: new RegExp(location, 'i') } }, { $set: weatherData });

      return res.send({ message: 'Weather data updated successfully', data: weatherData });
    } else {
      // If location doesn't exist, create a new document with the weather data
      const response = await axios.get(apiUrl, {
        params: {
          q: location,
          appid: apiKey,
        },
      });

      const weatherData = response.data;
      const { lon, lat } = weatherData.coord;

      weatherData.location = {
        type: 'Coordinate',
        coordinates: [lon, lat],
      };
      delete weatherData.coord;

      const newWeatherData = await monsoondata.create(weatherData);

      return res.send({ message: 'Weather data created successfully', data: newWeatherData });
    }
  } catch (error) {
    return res.status(500).send({ error: 'Error fetching or updating weather data' });
  }
};
