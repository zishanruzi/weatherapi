const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const monsoonSchema = new mongoose.Schema({
    location: {
        type: { type: String, default: 'Coordinate' },
        coordinates: [Number],
      },
      weather: [
        {
          id: { type: Number, default:null },
          main: { type: String, default:null },
          description: { type: String, default:null },
          icon: { type: String, default:null },
        },
      ],
      base: { type: String, default:null },
      main: {
        temp: { type: Number, default:null },
        feels_like: { type: Number, default:null },
        temp_min: { type: Number, default:null },
        temp_max: { type: Number, default:null },
        pressure: { type: Number, default:null },
        humidity: { type: Number, default:null },
        sea_level: { type: Number, default:null },
        grnd_level: { type: Number, default:null },
      },
      visibility: { type: Number, default:null },
      wind: {
        speed: { type: Number, default:null },
        deg: { type: Number, default:null },
        gust: { type: Number, default:null },
      },
      rain: {
        "1h": { type: Number, default:null },
      },
      clouds: {
        all: { type: Number, default:null },
      },
      dt: { type: Date, default:null },
      sys: {
        type: { type: Number, default:null },
        id: { type: Number, default:null },
        country: { type: String, default:null },
        sunrise: { type: Date, default:null },
        sunset: { type: Date, default:null },
      },
      timezone: { type: Number, default:null },
      id: { type: Number, default:null },
      name: { type: String, default:null },
      cod: { type: Number, default:null },
      
    }, {timestamps:true,
versionKey:false});
    
    
module.exports = mongoose.model('monsoon', monsoonSchema);